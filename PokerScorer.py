import numpy as np
from itertools import combinations
from Deck import SUITS, CARDS

SCORE_STRAIGHT_FLUSH = 10e17
SCORE_POKER = 10e16
SCORE_FULL_HOUSE = 10e15
SCORE_FLUSH = 10e14
SCORE_STRAIGHT = 10e13
SCORE_THREE = 10e12
SCORE_TWO_PAIR = 10e11
SCORE_PAIR = 10e10
SCORE_NAKED_CARDS = np.array([10e8, 10e6, 10e4, 10e2, 10e0], dtype=int)

SCORES_STRINGS = [
    (SCORE_STRAIGHT_FLUSH, 'Straight flush, {}'),
    (SCORE_POKER, 'Four of a kind, {}, kicker {}'),
    (SCORE_FULL_HOUSE, 'Full House, {} {}'),
    (SCORE_FLUSH, 'Flush, {}'),
    (SCORE_STRAIGHT, 'Straight, {}'),
    (SCORE_THREE, 'Three of a kind, {}, kicker {}'),
    (SCORE_TWO_PAIR, 'Two Pair, {} {}'),
    (SCORE_PAIR, 'Pair, {}, kicker {}'),
    (SCORE_NAKED_CARDS[0], 'High card, {}, second high {}')
]


class ScorerException(Exception):
    pass


class PokerScorer:

    def __init__(self):
        pass

    @staticmethod
    def score(cards):
        if len(cards) <= 5:
            return PokerScorer._score(cards)
        outs = []
        for card_combination in combinations(cards, 5):
            outs.append(PokerScorer._score(card_combination))
        return max(outs)

    @staticmethod
    def _score(cards):
        score_grid = np.zeros((len(SUITS), len(CARDS)), dtype=int)
        for card, suit in cards:
            score_grid[SUITS.index(suit), CARDS.index(card)] = 1

        card_counts = score_grid.sum(axis=0)
        suit_counts = score_grid.sum(axis=1)

        top_card_count, second_top_card_count = card_counts[card_counts.argsort()[::-1][:2]]

        top_suit_idx = suit_counts.argmax()
        top_suit_count = suit_counts[top_suit_idx]

        card_idxs = np.nonzero(card_counts)[0][::-1]
        top_count_idxs = card_counts.argsort()[::-1][:2]
        card_idxs = card_idxs[np.invert(np.in1d(card_idxs, top_count_idxs))]
        card_idxs = np.append(top_count_idxs, card_idxs)

        naked_card_scores = card_idxs * SCORE_NAKED_CARDS[:card_idxs.shape[0]]

        if 5 == top_suit_count:
            is_straight = 5 == np.max(np.convolve(score_grid[top_suit_idx, :], np.ones((5,))))
            if is_straight:
                return int(SCORE_STRAIGHT_FLUSH + naked_card_scores[0])
        if 4 == top_card_count:
            return int(SCORE_POKER + naked_card_scores[:2].sum())
        if 3 == top_card_count and 2 == second_top_card_count:
            return int(SCORE_FULL_HOUSE + naked_card_scores[:2].sum())
        if 5 == top_suit_count:
            return int(SCORE_FLUSH + naked_card_scores.sum())
        if 1 == top_card_count:
            is_straight = 5 == np.max(np.convolve(card_counts, np.ones((5,))))
            if is_straight:
                return int(SCORE_STRAIGHT + naked_card_scores[0])
        if 3 == top_card_count:
            return int(SCORE_THREE + naked_card_scores[:3].sum())
        if 2 == top_card_count and 2 == second_top_card_count:
            return int(SCORE_TWO_PAIR + naked_card_scores[:3].sum())
        if 2 == top_card_count:
            return int(SCORE_PAIR + naked_card_scores[:4].sum())
        return int(naked_card_scores.sum())

    @staticmethod
    def print_scores(score_number):
        card_1st_idx = int((score_number % SCORE_PAIR) // SCORE_NAKED_CARDS[0])
        card_2nd_idx = int((score_number % SCORE_NAKED_CARDS[0]) // SCORE_NAKED_CARDS[1])
        for score_cutoff, string in SCORES_STRINGS:
            if score_number >= score_cutoff:
                return string.format(CARDS[card_1st_idx], CARDS[card_2nd_idx])
