from Player import Player
from Deck import Deck
from collections import Counter, deque


class Table:

    def __init__(self, n_players, preset_cards):
        self._players = [Player(idx) for idx in range(n_players)]
        self._game = None
        self._round = 0
        self._winner_cards = deque()
        self._preset_cards = preset_cards

    def play(self, game, verbose=False):
        self._game = game(self._players, round_n=self._round, preset_cards=self._preset_cards, verbose=verbose)
        winner = self._game.play()
        while winner == -1:
            self._reset()
            self._game = game(self._players, round_n=self._round, verbose=verbose)
            winner = self._game.play()
            self._round += 1
        self._restack()
        winner_cards = Deck.hole_cards_shorthand(self._players[winner].hand_history[-1])
        self._winner_cards.append(winner_cards)

    def stats(self):
        l = len(self._winner_cards)
        for combo, count in Counter(self._winner_cards).most_common(10):
            print('{} - {}%'.format(combo, 100.0 * count / l))

    def _reset(self):
        for p in self._players:
            p.reset()

    def _restack(self):
        for p in self._players:
            p.restack()
