from Deck import Deck

DEFAULT_STACK = 1000
MAX_HISTORY = 5


class NaiveStrategy:
    def __init__(self, bet_amount=100):
        self._bet_amount = bet_amount
        pass

    def act(self, player, *args):
        return {
            'act': 'bet',
            'bet': player.bet(self._bet_amount),
        }


class AllInStrategy:
    def __init__(self):
        pass

    def act(self, player):
        return {
            'act': 'bet',
            'bet': player.bet(Player.ALL_IN),
        }


class Player:

    ALL_IN = 'all_in'

    def __init__(self, idx, strategy=AllInStrategy(), stack=DEFAULT_STACK):
        self.cards = []
        self.hand_history = []
        self.score = 0
        self.idx = idx
        self._stack = stack
        self._strategy = strategy

    def __repr__(self):
        return 'P{}:{}:${}'.format(
            self.idx,
            Deck.hole_cards_shorthand(self.cards),
            self._stack,
        )

    def act(self, *args):
        return self._strategy.act(self, *args)

    def get_stack(self):
        return self._stack

    def can_play(self):
        return self._stack > 0

    def cash(self, amount):
        self._stack += amount

    def reset(self):
        self.score = 0
        self.hand_history.append(self.cards)
        self.hand_history = self.hand_history[-MAX_HISTORY:]
        self.cards = []

    def restack(self, stack=DEFAULT_STACK):
        self._stack = stack

    def bet(self, amount):
        if amount is self.ALL_IN:
            amount = self._stack
        if amount <= self._stack:
            self._stack -= amount
        else:
            amount = self._stack
            self._stack = 0
        return amount

