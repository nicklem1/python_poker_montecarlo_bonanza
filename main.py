from Table import Table
from Game import TexasHoldEm


table = Table(n_players=4, preset_cards=('AAu', 'KTu', 'KTs', 'KKu'))

for _ in range(100):
    table.play(TexasHoldEm, verbose=False)

table.stats()
