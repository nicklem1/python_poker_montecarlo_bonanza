import itertools
import random
from collections import deque

CARDS = ('2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A')
SUITS = ('w', 'x', 'y', 'z')

IDX_CARD = 0
IDX_SUIT = 1


class DeckException(Exception):
    OUT_OF_CARDS = 'Out of cards'
    WRONG_FORMAT = 'Wrong input format'


class Deck:

    def __init__(self, seed=False, shuffle=True):
        self._deck = deque(''.join(c) for c in itertools.product(CARDS, SUITS))
        self._dealt = deque()
        if seed is not False and type(seed) is int:
            random.seed(seed)
        if shuffle:
            self.shuffle()

    def shuffle(self):
        random.shuffle(self._deck)

    def deal(self, n_cards=1):
        if n_cards > len(self._deck):
            raise DeckException(DeckException.OUT_OF_CARDS)
        cards = []
        for _ in range(n_cards):
            cards.append(self._deck.popleft())
        self._dealt.extend(cards)
        return cards

    def cards_left(self):
        return len(self._deck)

    def cards_dealt(self):
        return len(self._dealt)

    def draw_n_equal(self, card_id, how_many=2, reshuffle=True):
        return self._draw_n_type(IDX_CARD, card_id, how_many, reshuffle)

    def draw_same_suit(self, suit_id, how_many=5, reshuffle=True):
        return self._draw_n_type(IDX_SUIT, suit_id, how_many, reshuffle)

    def draw_these(self, cards, reshuffle=True):
        """
        >>> d = Deck()
        >>> cards = ('Ax', 'Kx')
        >>> assert d.draw_these(cards) == cards
        >>> d.draw_these(cards) # Can't redraw same cards
        Traceback (most recent call last):
            ...
        DeckException: Out of cards
        >>> cards = ('Ax', 'Ax')
        >>> d.draw_these(cards) # Can't draw same card twice (e.g. suited Aces)
        Traceback (most recent call last):
            ...
        DeckException: Out of cards
        >>> d.draw_these('AAs') # Shorthand test for above case
        Traceback (most recent call last):
            ...
        DeckException: Out of cards
        >>> cards = d.draw_these('JTu')
        >>> assert len(cards) == 2
        >>> assert cards[0][0] == 'J'
        >>> assert cards[1][0] == 'T'
        >>> assert cards[0][1] in SUITS
        >>> assert cards[1][1] in SUITS
        >>> assert cards[1][1] != cards[0][1]

        :param cards: Shorthand card notation, e.g. 'AKs'
        :return: Long form card tuple, e.g. ('Ax', 'Kx')
        """
        if 's' in cards or 'u' in cards:
            cards = self._extend_shorthand(cards)
        for c in cards:
            if c not in self._deck:
                raise DeckException(DeckException.OUT_OF_CARDS)
        for c in cards:
            self._deck.remove(c)
        if reshuffle:
            self.shuffle()
        self._dealt.extend(cards)
        return cards

    def draw_consecutive_suit(self, card_id, reshuffle=True):
        lower_card_id = CARDS[CARDS.index(str(card_id))-1]
        cards = list(filter(
            lambda card: (
                    card[IDX_CARD] == str(card_id) or
                    card[IDX_CARD] == str(lower_card_id)
            ), self._deck)
        )
        for c in cards:
            c2 = list(filter(
                lambda card: (
                        card[IDX_CARD] != c[IDX_CARD] and
                        card[IDX_SUIT] == c[IDX_SUIT]
                ), cards)
            )
            if len(c2):
                drawn = c, c2[0]
                if reshuffle:
                    self.shuffle()
                self._dealt.extend(drawn)
                return drawn
        raise DeckException(DeckException.OUT_OF_CARDS)

    @staticmethod
    def hole_cards_shorthand(cards):
        if len(cards) == 2:
            short_repr = ''.join(c for (c, _) in sorted(cards, key=lambda c: -CARDS.index(c[0])))
            short_repr += 's' if cards[0][1] == cards[1][1] else 'u'
        else:
            short_repr = '---'
        return short_repr

    def _draw_n_type(self, type_cat, type_id, how_many, reshuffle):
        cards = list(filter(lambda card: card[type_cat] == str(type_id), self._deck))[:how_many]
        if len(cards) < how_many:
            raise DeckException(DeckException.OUT_OF_CARDS)
        for c in cards:
            self._deck.remove(c)
        if reshuffle:
            self.shuffle()
        self._dealt.extend(cards)
        return cards

    def _extend_shorthand(self, cards):
        """
        >>> cards = Deck()._extend_shorthand('KTu')
        >>> assert len(cards) == 2
        >>> assert cards[0][0] == 'K'
        >>> assert cards[1][0] == 'T'
        >>> assert cards[0][1] in SUITS
        >>> assert cards[1][1] in SUITS
        >>> assert cards[0][1] != cards[1][1]
        >>> cards = Deck()._extend_shorthand('AKs')
        >>> assert len(cards) == 2
        >>> assert cards[0][0] == 'A'
        >>> assert cards[1][0] == 'K'
        >>> assert cards[0][1] in SUITS
        >>> assert cards[1][1] in SUITS
        >>> assert cards[0][1] == cards[1][1]

        :param cards: Shorthand card notation, e.g. 'AKs'
        :return: Long form card tuple, e.g. ('Ax', 'Kx')
        """
        (c1, c2), suited = cards[:2], cards[2] == 's'
        c1, c2 = [list(filter(lambda card: card[0] == my_card, self._deck)) for my_card in (c1, c2)]
        for cs1 in c1:
            cs2 = list(filter(lambda card: (card[1] == cs1[1]) == suited, c2))
            if len(cs2) and cs1 != cs2[0]:
                return cs1, cs2[0]
        raise DeckException(DeckException.OUT_OF_CARDS)


if __name__ == '__main__':
    import doctest
    doctest.testmod()

