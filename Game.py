from PokerScorer import PokerScorer
from Deck import Deck, SUITS


class TexasHoldEm:

    def __init__(self, players, round_n=0, preset_cards=(), verbose=False):
        self._deck = Deck()
        self._shared_cards = []
        self._scorer = PokerScorer()
        self._verbose = verbose
        self._round_n = round_n
        self._btn_idx = round_n % len(players)
        self._players = [
            p for p in
            players[self._btn_idx:] + players[:self._btn_idx]
            if p.can_play()
        ]
        self._pots = []
        self._preset_cards = preset_cards

    def play(self):
        if len(self._players) == 1:
            return self._players[0].idx
        self._pots.append(0)
        for idx, cards in enumerate(self._preset_cards):
            if len(self._players) > idx:
                self._players[idx].cards.extend(self._deck.draw_these(cards))
        for _ in range(2):
            for p in self._players[len(self._preset_cards):]:
                p.cards.extend(self._deck.deal())
        self._score_hands()
        if self._verbose:
            self._print_hand()
            input()
        self._bet_round()
        while self._play_round():
            if self._verbose:
                input()
            self._bet_round()
        winners = self._winners()
        win_each = int(self._pots[0] / len(winners))
        for p in winners:
            p.cash(win_each)
        return -1

    def _play_round(self):
        if 5 == len(self._shared_cards):  # We're done
            return False
        if len(self._shared_cards):
            self._shared_cards.extend(self._deck.deal())
        else:
            self._shared_cards = self._deck.deal(3)
        self._score_hands()
        if self._verbose:
            self._print_hand()
        return True

    def _bet_round(self):
        for p in self._players:
            act = p.act()
            act, bet = act['act'], act['bet']
            self._pots[0] += bet
            if self._verbose:
                print('Player {}: {}, ${}'.format(
                    p.idx, act, bet
                ))
        if self._verbose:
            print()

    def _score_hands(self):
        for p in self._players:
            p.score = self._scorer.score(p.cards + self._shared_cards)

    def _winners(self):
        sorted_players = sorted(self._players, key=lambda p: -p.score)
        max_score = sorted_players[0].score
        return list(filter(lambda p: p.score == max_score, sorted_players))

    def _print_hand(self):
        if len(self._shared_cards):
            print('Table: [{}]'.format(' '.join(self._shared_cards)))
            print('Pot(s): $( {} )'.format(', '.join(map(str, self._pots))))
            print()
        win_idxs = [p.idx for p in self._winners()]
        for p in self._players:
            print('{} - P{} (${:5}) [{}] - {}'.format(
                '*' if p.idx in win_idxs else ' ',
                p.idx,
                p.get_stack(),
                ' '.join(p.cards),
                self._scorer.print_scores(p.score),
            ))
